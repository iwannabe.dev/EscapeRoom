class Item:
    def __init__(self, name, position: list[int, int], description: str):
        self.name = name
        self.position = position
        self.description = description

class Room:
    def __init__(self, name, room_dimension: list[int, int],
                 door_location: list[int, int], items: list[type[Item]]):
        self.name = name
        self.room_dimension = room_dimension
        self.door_location = door_location
        self.items = items
        self.generate_map()

    def generate_map(self):
        self.map_of_room = [[u"\u2588" for i in range(1, self.room_dimension[0]+1)]
                    for j in range(self.room_dimension[1])]

    def show_map(self):
        x_row_length = len(self.map_of_room[0])
        print(" ", end='')
        for i in range(1, x_row_length + 1):
            print(i, end='')
        print('\r')
        y_row_number = 1
        for row in self.map_of_room:
            print(y_row_number, end='')
            for i in row:
                print(i, end='')
            print('\r')
            y_row_number += 1
        print("""\n    key:
            O - player's current position
            X - visited area
            \u2588 - area to be explored""")

    def mark_on_map(self, position: list[int, int], mark_sign: str):
        self.map_of_room[position[0]-1][position[1]-1] = mark_sign

    def is_within_room_dimension(self, position: list[int, int]) -> bool:
        if (position[0] < 0 or position[0] > self.room_dimension[0]
           or position[1] < 0 or position[1] > self.room_dimension[1]):
            return False
        else:
            return True

    def return_item_if_at_given_position(self, position: list[int, int]):
        for item in self.items:
            if position == item.position:
                return item

    @property
    def room_dimension(self):
        return self._room_dimension
    
    @room_dimension.setter
    def room_dimension(self, dimension: list[int, int]):
        if (dimension[0] > 1 and dimension[0] < 51
            and dimension[1] > 1 and dimension[1] < 51):
            self._room_dimension = dimension
        else:
            print("Minimum room dimension is 1x1 and maximum room dimension is 50x50!")

    @property
    def door_location(self):
        return self._door_location

    @door_location.setter
    def door_location(self, location: list[int, int]):
        door_location_x_axis = location[0]
        door_location_y_axis = location[1]
        if self.is_within_room_dimension(location):
            if door_location_x_axis == 0 or door_location_y_axis == 0:
                self._door_location = location
            else:
                    print("Door must be located on the wall")
        else:
            print("Location of the door must be within dimension of the room!")

    @property
    def items(self):
        return self._items

    @items.setter
    def items(self, items):
        validated_items = []
        for item in items:
            if self.is_within_room_dimension(item.position):
                validated_items.append(item)
            else:
                print(f"Item '{item.name}' is located outside of the room!")
        self._items = validated_items

    def remove_item(self, item):
        self.items.remove(item)

class Player:
    def __init__(self, name, inventory: list[Item] = [], position = [1, 1]):
        self.name = name
        self.inventory = []
        self.position = position

    def add_to_inventory(self, item):
        self.inventory.append(item)
        print(f"{item.name} has been added to your inventory")

    def remove_from_inventory(self, item):
        self.inventory.remove(item)

    def show_inventory(self, description_enabled: bool = False):
        if len(self.inventory) > 0:
            print("Your inventory contains: ")
            item_number = 1
            for item in self.inventory:
                if description_enabled:
                    print(f"{item_number}. {item.name} - {item.description}")
                else:
                    print(f"{item_number}. {item.name}")
                item_number += 1
        else:
            print("Your inventory is empty. Search the current room to find some useful items. \n")

    def move(self, given_direction: str, room: type[Room]):
        previous_position = [self.position[0], self.position[1]]
        if given_direction == "down":
            self.position[0] += 1
        elif given_direction == "up":
            self.position[0] -= 1
        elif given_direction == "left":
            self.position[1] -= 1
        elif given_direction == "right":
            self.position[1] += 1
        if not room.is_within_room_dimension(self.position):
            print("You can't move through walls! ...can you?")
            self.position = previous_position
            
        if (self.position[0] == 0 or self.position[1] == 0
            or self.position[0] >= room.room_dimension[0]
            or self.position[1] >= room.room_dimension[1]):
            print("You can't step onto a wall.")
            self.position = previous_position
        
def help_command():
    print("""List of commands:
        - 'help' - to show this list containing all availabe commands
        - 'move' - to move player. Must be followed by a direction, ie. 'move forward':
            'up' to move ↑
            'down' to move ↓
            'left' to move ←
            'right' to move →
        - 'show map' - to show a map with current progress of room's exploration
        - 'show inventory' - to show all items you've found so far
        - 'inspect inventory' - to show details about all items in your inventory
        - 'exit' \ """
          )

def exit_command():
    print("GAME OVER! CU NEXT TIME.")
    exit()

def game_intro():
    print("You're locked an a room and your goal is to find a way out. "
          + "'Move' around, 'take' as many items you can find and 'inspect' "
          + "them for some clues.")

def move(given_direction, current_player: type[Player], current_room: type[Room]):
    current_room.mark_on_map(current_player.position, "X")
    given_directions_list = ["up", "down", "left", "right"]
    if given_direction not in given_directions_list:
        command_not_recognized()
    else:
        current_player.move(given_direction, current_room)
        item_found = current_room.return_item_if_at_given_position(current_player.position)
        if item_found is not None:
            print(f"You just found: {item_found.name}")
            if item_found.name == "door":
                door_found()
            else:
                current_player.add_to_inventory(item_found)
                current_room.remove_item(item_found)
        current_room.mark_on_map(current_player.position, "O")

def door_found():
    print("The door is locked with an electronic pinpad. Please enter a PIN and "
          +"confirm with 'ENTER'")
    remaining_attempts = 3
    while remaining_attempts > 0:
        pin = input("PIN >")
        if pin == "9753":
            print("Hooray! You unlock the door and escaped the room!")
            exit_command()
            break
        else:
            remaining_attempts -=1
            print(f"Wrong PIN. You have {remaining_attempts} remaining attempts.")
            if remaining_attempts == 0:
                print("Come back later and try again. In the meantime you can "
                      + "search the room for any clues")
            
def command_not_recognized():
    print("I don't understand. Type 'help' to show all available commands.")

def clear_console():
    print("\n" * 40)

commands_list = ["help", "exit", "move up", "move down", "move left",
                 "move right", "show map", "show inventory", "inspect inventory"]

suitcase = Item("suitcase", [3, 3], "old suitcase with a piece of paper inside "
                + "that has some handwritten note: 9753")
cellphone = Item("cellphone", [3, 4], "old Nokia 3310 cellphone. Batery 1%, please charge")
vase = Item("vase", [1, 3], "ugly vase")
pinboard = Item("pinboard", [4, 4], "pinboard with a note: password 13579")
door = Item("door", [6, 1], "locked door with an electronic pinpad on attached. "
            + "It has an old type 7-segment display which can show up to 4 digits.")
items = [suitcase, cellphone, vase, pinboard, door]

kuchnia = Room("Kitchen", [9, 8], [6, 0], items)

zawodnik = Player("Jan")
current_player = zawodnik
current_room = kuchnia

game_intro_played = False

# game loop
while True:
    if game_intro_played == False:
        game_intro()
        game_intro_played = True
    else:        
        prompt = "Enter a command: \n> "
        user_input = input(prompt)
        if user_input not in commands_list:
            command_not_recognized()
        else:
            clear_console()
            if user_input == "help":
                help_command()
            elif user_input == "exit":
                exit_command()
                break
            elif user_input.split()[0] == "move":
                given_direction = user_input.split()[1]
                move(given_direction, current_player, current_room)
            elif user_input == "show map":
                current_room.show_map()
            elif user_input == "show inventory":
                current_player.show_inventory()
            elif user_input == "inspect inventory":
                current_player.show_inventory(description_enabled = True)
    
